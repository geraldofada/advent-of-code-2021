use std::{env, fs};

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Please provide the input file.");
    }

    let file_path = &args[1];

    let contents: String = fs::read_to_string(file_path).unwrap().trim().to_string();
    let parsed_contents: Vec<&str> = contents.split("\n").collect();
    if parsed_contents.len() == 0 {
        println!("0");
    }

    let mut zeros: Vec<i32> = vec![0; parsed_contents[0].chars().count().try_into().unwrap()];
    let mut ones: Vec<i32> = vec![0; parsed_contents[0].chars().count().try_into().unwrap()];

    for i in parsed_contents {
        for (i, s) in i.chars().enumerate() {
            if s == '0' {
                zeros[i] += 1;
            } else {
                ones[i] += 1;
            }
        }
    }

    let mut gamma: String = "".to_string();
    let mut epsilon: String = "".to_string();

    for (i, _) in zeros.iter().enumerate() {
        if zeros[i] > ones[i] {
            gamma.push('0');
            epsilon.push('1');
        } else {
            gamma.push('1');
            epsilon.push('0');
        }
    }

    println!(
        "{}",
        i64::from_str_radix(gamma.as_str(), 2).unwrap()
            * i64::from_str_radix(epsilon.as_str(), 2).unwrap()
    );
}
