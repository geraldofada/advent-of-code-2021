use std::{env, fs};

struct Position {
    horizontal: i32,
    depth: i32,
    aim: i32,
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Please provide the input file.");
    }

    let file_path = &args[1];

    let contents: String = fs::read_to_string(file_path).unwrap().trim().to_string();
    let parsed_contents: Vec<&str> = contents.split("\n").collect();
    if parsed_contents.len() == 0 {
        println!("0");
    }

    let mut pos = Position {
        horizontal: 0,
        depth: 0,
        aim: 0,
    };

    for i in parsed_contents {
        let i_splited: Vec<&str> = i.split(" ").collect();
        if i_splited.len() < 2 {
            panic!("Invalid input");
        }

        let direction: &str = i_splited[0];
        let units: i32 = i_splited[1].parse::<i32>().unwrap();
        match direction {
            "forward" => {
                pos.horizontal += units;
                pos.depth += units * pos.aim;
            }
            "down" => pos.aim += units,
            "up" => pos.aim -= units,
            _ => (),
        }
    }

    println!("{}", pos.horizontal * pos.depth);
}
