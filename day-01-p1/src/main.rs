use std::{env, fs};

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Please provide the input file.");
    }

    let file_path = &args[1];

    let contents: String = fs::read_to_string(file_path).unwrap().trim().to_string();
    let parsed_contents: Vec<&str> = contents.split("\n").collect();
    if parsed_contents.len() == 0 {
        println!("0");
    }

    let mut incs: i32 = 0;
    let mut latest: i32 = parsed_contents[0].parse::<i32>().unwrap();
    for i in parsed_contents {
        let i_parsed: i32 = i.parse::<i32>().unwrap();
        if i_parsed > latest {
            incs += 1;
        }
        latest = i.parse::<i32>().unwrap();
    }

    println!("{incs}");
}
