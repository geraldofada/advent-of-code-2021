use std::{env, fs};

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Please provide the input file.");
    }

    let file_path = &args[1];
    let window_size: usize = 3;

    let contents: String = fs::read_to_string(file_path).unwrap().trim().to_string();
    let parsed_contents: Vec<&str> = contents.split("\n").collect();
    if parsed_contents.len() <= window_size {
        println!("0");
        return;
    }

    let mut window_sums: Vec<i32> = vec![];
    let max_windows: usize = parsed_contents.len() - window_size + 1;
    for i in 0..max_windows {
        let mut current_sum = 0;
        for k in i..(i + window_size) {
            current_sum += parsed_contents[k].parse::<i32>().unwrap();
        }

        window_sums.push(current_sum);
    }

    let mut incs: i32 = 0;
    let mut latest: i32 = window_sums[0];
    for i in window_sums {
        if i > latest {
            incs += 1;
        }
        latest = i;
    }

    println!("{incs}");
}
