### Advent of Code 2021

This repo contains solutions for the Advent of Code 2021 written in Rust.

You can read more about [here](https://adventofcode.com/2021/about).
